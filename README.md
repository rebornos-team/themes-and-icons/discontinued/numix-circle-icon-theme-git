# numix-circle-icon-theme-git

Circle icon theme from the Numix project

How to clone this repo:

```
git clone https://gitlab.com/reborn-os-team/themes-and-icons/numix-circle-icon-theme-git.git
```

